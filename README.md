# Universal JSON Settings Classes

## Settings handling for JSON formatted classes

Base for using JSON formatted settings in class structure
Usage is intended for python/robotframework tho probably can be used elsewhere as well.
Idea is to simplify usage on robot side to allow simple keywords to be used

## Current usage

Currently used by cloning repo or copying code, don't really care one way or another. If a lot of usage starts to happen will probably create a pip package of it.

## Usage
Example usage can be found under examples

## Support
Contact me one way or another if support is needed.

## License
Use however, copy the code, fork it or whatever. Will add proper license at some point, but will remain open source.

## Project status
Just started this, so chill. might contribute more or less to improve it when i have more time.
