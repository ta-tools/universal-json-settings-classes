import json
import jsonpickle
from base_functions import (
    change_value,
    get_value,
    verify_settings_match
)

class Settings(object):
    """
    Common settings class for containing common functions and variables
    """

    _rest_endpoint = ""
    _rest_get_headers = {
        "Content-type": "application/json",
        "Accept": "text/plain",
    }    
    _rest_set_headers = {
        "Content-type": "application/json",
        "Accept": "text/plain",
    }
    _rest_set_method = "PUT"
    _mqtt_topic = ""

    @property
    def rest_endpoint(self):
        """
        Returns rest endpoint if set

        Returns:
            str: Rest endpoint
        """
        return self._rest_endpoint

    @property
    def get_headers(self):
        """
        Returns required headers for GET request if set

        Returns:
            dict: Headers for GET request
        """
        return self._rest_get_headers

    @property
    def set_headers(self):
        """
        Returns required headers for {rest_set_method} request if set

        Returns:
            dict: Headers for {rest_set_method} request
        """
        return self._rest_set_headers

    @property
    def rest_set_method(self):
        """
        Returns rest set method

        Returns:
            str: Rest method to set settings
        """
        return self._rest_set_method

    @property
    def mqtt_topic(self):
        return self._mqtt_topic

    def change_value(self, key, value):
        """
        Changes settings key to value

        Args:
            key (str):          key to change
            value (any):        value

        Raises:
            ValueError:         If key not found
        """
        if not change_value(self, key, value):
            raise ValueError(
                f"Couldn't change {key} to {value} in settings class"
            )

    def get_value(self, key):
        """
        Returns settings value for key

        Args:
            key (str):          key to get

        Raises:
            ValueError:         If key not found
        """
        success, value = get_value(self, key)
        if success:
            return value
        raise ValueError(f"{key} not found in settings")

    def json(self):
        """
        Returns settings in json format

        Returns:
            str: json formatted settings
        """
        return jsonpickle.encode(self, unpicklable=False)

    def dict(self):
        """
        Returns settings in dict format

        Returns:
            dict: dict formatted settings
        """
        return json.loads(self.json())

    def verify_settings_match(
        self, __value: object, *ignore_keys, **special_verifications
    ):
        """
        Verifies settings match given settings class

        Args:
            __value (Settings):         Settings to verify against
            *ignore_keys:               keys to ignore during verification
            **special_verifications:    Special functions to use for specified keys

        Raises:
            AttributeError:             If verification errors found
        """
        success, errors = verify_settings_match(
            self, __value, *ignore_keys, **special_verifications
        )
        if not success:
            raise AttributeError(errors)

    def __eq__(self, __value: object) -> bool:
        return verify_settings_match(self, __value)[0]

    def __ne__(self, __value: object) -> bool:
        return verify_settings_match(self, __value)[0]