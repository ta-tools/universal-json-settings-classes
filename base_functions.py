
def is_numeric(value):
    """
    Checks if value is numeric.
    This is to prevent issues when using variables from RobotFramework
    """
    try:
        float(value)
        return True
    except ValueError:
        return False

def is_true(value):
    """
    Evaluates if value is True.
    This is to prevent issues when using variables from RobotFramework.
    
    This is not used by default, you need to add this if needed.
    """
    _truthy = [True, "True", "true", 1, "1"]
    return value in _truthy

def change_value(obj, key, value):
    """
    Changes value in settings
    
    Args:
        obj (Settings):     Settings to check from
        key (str):          key
        value (Any):        value
    
    Returns:
        bool:               Success of operation
    """
    if "." in key:
        _parent, _child = key.split(".", 1)
        if hasattr(obj, "__name__"):
            _parent_verification = obj.__name__.lower()
        else:
            _parent =  _parent.replace("_", "")
            _parent_verification = type(obj).__name__.lower()
        if _parent_verification == _parent.lower() or _parent_verification == _parent.lower().replace("_", ""):
            _success = change_value(obj, _child, value)
            if _success:
                return _success
    if hasattr(obj, key):
        if isinstance(getattr(obj, key), type(value)):
            setattr(obj, key, value)
            return True
        elif is_numeric(value) and isinstance(getattr(obj, key), float):
            setattr(obj, key, float(value))
            return True
        elif is_numeric(value) and isinstance(getattr(obj, key), int):
            setattr(obj, key, int(value))
            return True

    for _, old_value in vars(obj).items():
        if hasattr(old_value, "__dict__"):
            _success = change_value(old_value, key, value)
            if _success:
                return _success
    return False


def get_value(obj, key):
    """
    Returns value for key in settings
    
    Args:
        obj (Settings):     Settings to check from
        key (str):          key
    
    Returns:
        bool:               Success of operation
        any:                value for key
    """
    if "." in key:
        _parent, _child = key.split(".", 1)
        if hasattr(obj, "__name__"):
            _parent_verification = obj.__name__.lower()
        else:
            _parent =  _parent.replace("_", "")
            _parent_verification = type(obj).__name__.lower()
        if _parent_verification == _parent.lower():
            _success, _value = get_value(obj, _child)
            if _success:
                return _success, _value
    if hasattr(obj, key):
        _value = getattr(obj, key)
        return True, _value
    for _, old_value in vars(obj).items():
        if hasattr(old_value, "__dict__"):
            _success, _value = get_value(old_value, key)
            if _success:
                return _success, _value
    return False, None

def _filter_ignores(obj, all_keys, ignore_keys):
    """
    Filters ignored keys

    Args:
        obj (Settings):         Settings that are being verified
        all_keys (list):        List of all keys to filter out of
        ignore_keys (list):     List of ignored keys

    Returns:
        _type_: _description_
    """
    _ignore_keys = list(ignore_keys).copy()
    for ignore in ignore_keys:
        if "." in ignore:
            _parent, _child = ignore.split(".", 1)
            if hasattr(obj, "__name__"):
                _parent_verification = obj.__name__.lower()
            else:
                _parent =  _parent.replace("_", "")
                _parent_verification = type(obj).__name__.lower()
            if _parent_verification == _parent.lower() and _child in all_keys:
                all_keys.remove(_child)
            elif _parent_verification == _parent.lower() and "." in _child:
                _ignore_keys.append(_child)
        elif ignore in all_keys:
            all_keys.remove(ignore)
    return all_keys, _ignore_keys

def _filter_special_verifications(obj, all_keys, special_verifications):
    """
    Filters keys to special verifications when using . notation

    Args:
        obj (Settings):                 Settings that are being verified
        all_keys (list):                List of all keys to verify on current level
        special_verifications (dict):   Special verifications for level

    Returns:
        list: all keys to verify
        dict: special verifications for current level
    """
    _special_verifications = special_verifications.copy()
    for special in special_verifications.keys():
        if "." in special:
            _parent, _child = special.split(".", 1)
            if hasattr(obj, "__name__"):
                _parent_verification = obj.__name__.lower()
            else:
                _parent =  _parent.replace("_", "")
                _parent_verification = type(obj).__name__.lower()
            if _parent_verification == _parent.lower() and _child in all_keys:
                _special_verifications[_child] = special_verifications[special]
            elif _parent_verification == _parent.lower() and "." in _child:
                _special_verifications[_child] = special_verifications[special]
    return all_keys, _special_verifications

def verify_settings_match(obj, obj2, *ignore_keys, **special_verifications):
    """
    Verifies that all values in obj are in obj2 and that they match

    Args:
        obj (object):                   Settings to verify
        obj2 (object):                  Settings to verify against
        *ignore_keys (str):             keys to ignore
        **special_verifications (dict): dict of special verifications

    Returns:
        bool:                           True if all values in obj are in obj2
        str:                            errors
    """
    errors = ""
    success = True
    _all_keys = set(list(vars(obj).keys()) + list(vars(obj2).keys()))
    _all_keys, _ignore_keys = _filter_ignores(obj, _all_keys, ignore_keys)
    _all_keys, _special_verifications = _filter_special_verifications(obj, _all_keys, special_verifications)

    for key in _all_keys:
        _success = True
        if key in _special_verifications.keys():
            _success, _errors = _special_verifications[key](
                getattr(obj, key), getattr(obj2, key)
            )
        elif not hasattr(obj2, key) or not hasattr(obj, key):
            _errors = f"{key} not found in settings\n"
            _success = False
        elif hasattr(getattr(obj, key), "__dict__"):
            _success, _errors = verify_settings_match(
                getattr(obj, key),
                getattr(obj2, key),
                *_ignore_keys,
                **_special_verifications,
            )
            if _errors:
                _errors = f"{key}: {_errors}"
        elif getattr(obj2, key) != getattr(obj, key):
            _errors = (
                f"{key} value {getattr(obj2, key)} "
                f"does not match {getattr(obj, key)}\n"
            )
            _success = False
        if not _success:
            errors += _errors
            success = False
    return success, errors
