from requests import Session
from example_keywords import (
    create_settings,
    get_value,
    change_value,
    verify_settings,
)

import time


class Rest:
    def __init__(self, url, port=80, timeout=10):
        if ":" in url:
            url, port = url.split(":")
        self.url = url
        self.port = port
        self._session = None

    def start_session(self):
        self._session = Session()

    def change_settings_value_on_device(self, settings_type, key, value):
        _settings = self.get_settings(settings_type)
        change_value(_settings, key, value)
        self.send_settings(_settings)

    def get_settings(self, settings_type):
        _settings = create_settings(settings_type)
        _endpoint = _settings.rest_endpoint
        _headers = _settings.rest_get_headers
        response = self._session.get(
            f"{self.url}:{self.port}{_endpoint}", headers=_headers
        )
        response.raise_for_status()
        return create_settings(settings_type, **response.json())

    def get_settings_value_from_device(self, settings_type, key):
        _settings = self.get_settings(settings_type)
        return get_value(_settings, key)

    def send_settings(self, settings):
        _endpoint = settings.rest_endpoint
        _headers = settings.rest_set_headers
        _method = settings.rest_set_method
        response = self._session.request(
            _method, f"{self.url}:{self.port}{_endpoint}", headers=_headers
        )
        response.raise_for_status()

    def verify_settings_match(self, settings, *ignore_keys):
        current_settings = self.get_settings(settings.alias)
        verify_settings(settings, current_settings, *ignore_keys)

    def wait_until_settings_match(self, settings, *ignore_keys, timeout=30):
        _start_time = time.monotonic()
        _end_time = _start_time + timeout
        while time.monotonic() < _end_time:
            try:
                self.verify_settings_match(settings, *ignore_keys)
                return
            except AttributeError:
                time.sleep(0.2)
        raise AttributeError(
            f"{settings.alias.title()} settings not updated"
            f" in {time.monotonic() - _start_time} seconds."
        )
