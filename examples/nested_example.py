"""
Example of creating class for settings with nested values
"""

example_json = """
{
    "first_key": 0,
    "second_key": {
        "nested_key": {
            "another_nested": {
                "just_a_key": 123
            }
        }
    }
}

"""

from base_class import Settings


class ExampleSettings(Settings):
    # Default values are set in class init
    # This can either be done for each level
    # or in base init.
    def __init__(self, first_key=0, second_key={}):
        self.first_key = first_key
        self.second_key = SecondKey(**second_key)


class SecondKey:
    def __init__(self, nested_key={}):
        self.nested_key = NestedKey(**nested_key)


class NestedKey:
    def __init__(self, another_nested={}):
        self.another_nested = AnotherNested(**another_nested)

class AnotherNested:
    def __init__(self, just_a_key=123):
        self.just_a_key = just_a_key
