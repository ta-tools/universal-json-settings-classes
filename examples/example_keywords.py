
from iterable_example import ExampleSettings as IterableSettings
from nested_example import ExampleSettings as NestedSettings


def create_settings(settings_type, **settings):
    _settings = {
        "iterable": IterableSettings,
        "nested": NestedSettings
    }
    return _settings[settings_type](**settings)

def change_value(settings, key, value):
    settings.change_value(key, value)

def get_value(settings, key):
    return settings.get_value(key)

def verify_settings(settings, settings2, *ignore_keys):
    settings.verify_settings_match(settings2, *ignore_keys)
