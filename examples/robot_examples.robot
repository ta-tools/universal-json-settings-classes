*** Settings ***
Library        examples/example_keywords.py


*** Test Cases ***
Verify Settings Examples With Nested Settings
    ${first_nested}    Create Settings    nested
    ${second_nested}    Create Settings    nested
    Verify Settings    ${first_nested}    ${second_nested}
    Change Value    ${first_nested}    just_a_key    333
    ${value1}    Get Value    ${first_nested}    just_a_key
    Should Be Equal As Integers    ${value1}    333
    ${should fail}    Run Keyword And Return Status    Verify Settings    ${first_nested}    ${second_nested}
    Should Not Be True    ${should fail}

Verify Settings Examples With Iterable Nested Settings
    ${first_iter}    Create Settings    iterable
    ${second_iter}    Create Settings    iterable
    Verify Settings    ${first_iter}    ${second_iter}
    Change Value    ${first_iter}    iter2.just_a_key    333
    ${value1}    Get Value    ${first_iter}    iter2.just_a_key
    Should Be Equal As Integers    ${value1}    333
    ${should fail}    Run Keyword And Return Status    Verify Settings    ${first_iter}    ${second_iter}
    Should Not Be True    ${should fail}
