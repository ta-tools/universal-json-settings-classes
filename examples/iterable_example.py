"""
Example of creating class for settings with iterable values
"""

example_json = """
{
    "first_key": 0,
    "second_key": {
        "nested_key": {
            "iter1": {
                "just_a_key": 123,
                "nested_iter1": {
                    "nested": {
                        "end": 77
                    }
                },
                "nested_iter2": {
                    "nested": {
                        "end": 77
                    }
                }
            },
            "iter2": {
                "just_a_key": 45,
                "nested_iter1": {
                    "nested": {
                        "end": 77
                    }
                },
                "nested_iter2": {
                    "nested": {
                        "end": 77
                    }
                }
            },
            "iter3": {
                "just_a_key": 66,
                "nested_iter1": {
                    "nested": {
                        "end": 77
                    }
                },
                "nested_iter2": {
                    "nested": {
                        "end": 77
                    }
                }
            }
        }
    }
}

"""

from base_class import Settings
from math import isclose

def example_special_verification(value1, value2):
    _errors = ""
    _success = True
    if not isclose(value1, value2, abs_tol=10):
        _success = False
        _errors = f"{value1} not close enough to {value2}. Tolerance: 10"
    return _success, _errors

class ExampleSettings(Settings):
    # Default values are set in class init
    # This can either be done for each level
    # or in base init.
    def __init__(self, first_key=0, second_key={}):
        self.first_key = first_key
        self.second_key = SecondKey(**second_key)
    
    def verify_settings_match(self, __value: object, *ignore_keys, **special_verifications):
        _special_verifications = {
            "just_a_key": example_special_verification
        }
        return super().verify_settings_match(__value, *ignore_keys, **_special_verifications)


class SecondKey:
    def __init__(self, nested_key={}):
        self.nested_key = NestedKey(**nested_key)

class NestedKey:
    def __init__(self, **iters):
        if not iters:
            iters = Iter._defaults
        for key, value in iters.items():
            if isinstance(value, dict):
                setattr(self, key, Iter(name=key, **value))
                continue
            setattr(self, key, value)

class Iter:
    # Example of setting differing defaults in iterable class
    _defaults = {
        "iter1": {
            "just_a_key": 123
        },
        "iter2": {
            "just_a_key":44
        },
        "iter3": {
            "just_a_key": 77
        }
    }
    def __init__(self, just_a_key=None, name="iter1", **nested_iter):
        if just_a_key is not None:
            self.just_a_key = just_a_key
        else:
            self.just_a_key = self._defaults.get(name)["just_a_key"]
        for key, value in nested_iter.items():
            if isinstance(value, dict):
                setattr(self, key, NestedIter(name=key, **value))
                continue
            setattr(self, key, value)
        self.__name__ = name
    
    def __getstate__(self) -> object:
        _dict = self.__dict__.copy()
        del _dict["__name__"]
        return _dict

class NestedIter:
    # Optional class that is not present by default
    def __init__(self, name="nested_iter1", **nested):
        self.__name__ = name
        for key, value in nested.items():
            setattr(self, key, Nested(**value))

    def __getstate__(self) -> object:
        _dict = self.__dict__.copy()
        del _dict["__name__"]
        return _dict

class Nested:
    def __init__(self, end=77):
        self.end = end
